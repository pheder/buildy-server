import * as jwt from 'jsonwebtoken';

import User from '../models/user';
import BaseCtrl from './base';

import {comparePassword} from '../utils'
import Build from '../models/build';


export default class UserCtrl extends BaseCtrl {
  model = User;


  login = (req, res) => {
    this.model.findOne({ email: req.body.email }, (err, user) => {
      if (!user) { return res.sendStatus(403); }
      comparePassword(req.body.password, user.password, (error, isMatch) => {
        if (!isMatch) { return res.sendStatus(403); }
        const token = jwt.sign({ user: user }, process.env.SECRET_TOKEN as string, { expiresIn: "3d" }); 
        res.status(200).json({ token: token, user: user });
      });
    });
  }

  validateUser = (req, res,next) =>{
    jwt.verify(req.headers["authorization"].split(' ')[1], process.env.SECRET_TOKEN as string, (err, decoded) =>{
      if(err){
        console.log("INVALID TOKEN");
        res.sendStatus(403);
      }
      else{
        req.params.userId = decoded.user._id;
        next();
      }
    });
  }

  getUserInfo = (req, res) =>{
    jwt.verify(req.headers["authorization"].split(' ')[1], process.env.SECRET_TOKEN as string, (err, decoded) =>{
      if(err){
        res.sendStatus(403);
      }
      else{
       res.send(decoded);
      }
    });
  }

  updateBuild = (req, res) => {
    this.model.findById( req.params.id ,  (err,doc) => {
      if (err) { return console.error(err); }
      if(!doc!.build){
        doc!.build = { name: "",  components: {}, price: 0 };
      }
      let type = req.body.type.replace('-', '_');
      if(type == "drive"){
        doc!.build.components[type].push({_id: req.body.component._id, manufacturer: req.body.component.manufacturer, model: req.body.component.model, price: req.body.component.price})
      }
      else{
          doc!.build.components[type] = {_id: req.body.component._id, manufacturer: req.body.component.manufacturer, model: req.body.component.model, price: req.body.component.price};
      }
      doc!.save(function (err) {
        if (err) { return console.error(err); }
        res.send(doc);
      })
   });
  }

  deleteBuildComponent = (req, res) => {
      this.model.findById( req.params.id ,  (err,doc) => {
      if (err) { return console.error(err); }
      if(!doc!.build){
        doc!.build = { name: "",  components: {}, price: 0 };
      }
      let type = req.params.type.replace('-', '_');
      if(type == "drive"){
        let index = doc!.build.components[type].findIndex(drive => {
          return drive._id == req.params.componentId;
        });
        doc!.build.components[type].splice(index, 1);
      }
      else{
        doc!.build.components[type] = undefined;
      }
      doc!.save(function (err) {
        if (err) { return console.error(err); }
        res.status(200).json({"success": true});
      })
    });
  }

  updateBuildName = (req, res) => {
      this.model.findById( req.params.id ,  (err,doc) => {
      if (err) { return console.error(err); }
      if(!doc!.build){
        doc!.build = { name: "",  components: {}, price: 0 };
      }
      doc!.build.name = req.body.name;
      doc!.build.description = req.body.description;
      doc!.save(function (err) {
        if (err) { return console.error(err); }
        res.send(doc);
      })
    });
  }

  finishBuild = async (req, res) => {

      const user =  await this.model.findById( req.params.id).exec();

      const build =  new Build({...user!.build, owner: {name: user!.username, _id: user!._id }, price: req.body.price });

      build.save((err, item) => {
        // 11000 is the code for duplicate key error
        if (err && err.code === 11000) {
          res.sendStatus(400);
        }
        if (err) {
          return console.error(err);
        }

        user!.build = { name: "",  components: {}, price: 0 };
        user!.save(function (err) {
          if (err) { return console.error(err); }
          res.send(build);
        })
    });

  }

  getBuild = (req, res) => {
    this.model.findOne({ _id:  req.params.id  }, 'build', (err, doc) => {
      if (err) { return console.error(err); }
      res.status(200).json(doc);
    });
  }
}
