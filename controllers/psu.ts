import Psu from '../models/psu';
import CommonCtrl from './common';
import { prepareFilters } from '../utils';


export default class cc extends CommonCtrl  {
  model = Psu;

  getAll = (req, res) => {
    this.model.find({}, 'manufacturer model price type power efficiency modular retailers images' ,(err, docs) => {
      if (err) { return console.error(err); }
      res.json(docs);
    });
  }


}
