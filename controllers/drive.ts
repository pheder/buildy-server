import Drive from '../models/drive';
import CommonCtrl from './common';
import { prepareFilters } from '../utils';


export default class cc extends CommonCtrl  {
  model = Drive;


  getAll = (req, res) => {
    this.model.find({}, 'manufacturer model price size type capacity interface retailers images' ,(err, docs) => {
      if (err) { return console.error(err); }
      res.json(docs);
    });
  }
}
