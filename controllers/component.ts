import Component from '../models/component';
import Retailer from '../models/retailer';
import BaseCtrl from './base';

import * as formidable from 'formidable';
import * as path from 'path';
import * as fs from 'fs';

import * as cheerio from 'cheerio';
import request from 'request';


export default class cc extends BaseCtrl {
  model = Component;


  validate = (req, res) => {
    var retailer = req.body;
      request(retailer.url, function (error, response, html) {
        if (!error && response.statusCode == 200) {
           var $ = cheerio.load(html);
           let price;
           switch(retailer.name){
             case 'worten':
              price = parseFloat($('div.price').attr('data-dle-price'));
              res.status(200).json(price);
             break;
             case 'alternate':
               var tmp = $('span[itemprop=price]').attr('content');
               tmp = tmp.replace(".", "");
               price = parseFloat(price.replace(",", "."));
               res.status(200).json(price);
             break;
             case 'xtremmedia':
               price = $('span[itemprop=price]').attr('content');
               price = parseFloat(price.replace(",", "."));
               res.status(200).json(price);
             break;
             case 'pccomponentes':
              price = $('#priceBlock').attr('data-price');
              price = parseFloat(price);
              res.status(200).json(price);
             break;
             case 'coolmod':
              price = $('#hidden_price').text();
              price = price.replace(".", "");
              price = parseFloat(price.replace(",", "."));
              res.status(200).json(price);
             break;
             case 'pixmania':
               price = $('span[itemprop=price]').attr('content');
               price = parseFloat(price);
               res.status(200).json(price);
             break;
             case 'mediamarkt':
               price = $('meta[name=finalPrice]').attr('content');
               price = parseFloat(price);
               res.status(200).json(price);
             break;
             default:
              res.status(466).json({error: "retailer name not recognized"});
             break;
           }
        }
        else{
          console.log(response);
        }
      });
  }

  upload = (req, res) => {
    var form = new formidable.IncomingForm();
   form.parse(req, function(err, fields, files) {

        var old_path = files.file.path,
            file_size = files.file.size,
            file_name = files.file.name,
            file_mime = files.file.type,
            rel_path ='/assets/images/components/' + file_name,
            new_path = path.join(process.env.ROOT_FOLDER as string , rel_path);

        if(file_mime.split("/")[0] !== "image"){
          res.status(500).json({'success': false, 'error' : 'Invalid image format'});
        }

        else if(file_size > 100000){
          res.status(500).json({'success': false, 'error' : 'The image is too big'});
        }

        else{
          fs.readFile(old_path, function(err, data) {
              fs.writeFile(new_path, data, function(err) {
                  fs.unlink(old_path, function(err) {
                      if (err) {
                          res.status(500);
                          res.json({'success': false});
                      } else {
                          res.status(200);
                          res.json({'success': true, 'src': rel_path});
                      }
                  });
              });
          });
        }
    });
  }
}
