import Case from '../models/case';
import CommonCtrl from './common';
import { prepareFilters } from '../utils';


export default class cc extends CommonCtrl  {
  model = Case;


  getAll = (req, res) => {
    this.model.find({}, 'manufacturer model price type exBig inBig inSmall retailers images' ,(err, docs) => {
      if (err) { return console.error(err); }
      res.json(docs);
    });
  }
}
