import Gpu from '../models/gpu';
import CommonCtrl from './common';
import { prepareFilters } from '../utils';

export default class gpuController extends  CommonCtrl{
  model = Gpu;

  getAll = (req, res) => {
    this.model.find({}, 'manufacturer model price clock chipset memory_size memory_type tdp ports sync retailers images' ,(err, docs) => {
      if (err) { return console.error(err); }
      res.json(docs);
    });
  }
}
