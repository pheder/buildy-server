import Cpu from '../models/cpu';
import CommonCtrl from './common';
import {prepareFilters} from '../utils';


export default class cc extends CommonCtrl  {
  model = Cpu;

  getAll = (req, res) => {
    this.model.find({}, 'manufacturer model price clock socket cores threads tdp igpu retailers images' ,(err, docs) => {
      if (err) { return console.error(err); }
      res.json(docs);
    });
  }

}
