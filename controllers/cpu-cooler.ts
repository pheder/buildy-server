import CpuCooler from '../models/cpu-cooler';
import CommonCtrl from './common';
import { prepareFilters } from '../utils';

export default class cc extends CommonCtrl  {
  model = CpuCooler;


  getAll = (req, res) => {
    this.model.find({}, 'manufacturer model price liquid speed sockets noise retailers images' ,(err, docs) => {
      if (err) { return console.error(err); }
      res.json(docs);
    });
  }

}
