import * as mongoose from 'mongoose'
import { prepareFilters } from '../utils';

abstract class CommonCtrl {

  abstract model:  mongoose.PaginateModel<any>

  get = (req, res) => {
    this.model.findOne({ _id: req.params.id }).populate('comments.author', 'username _id').exec( (err, doc) =>{
      if (err) { return console.error(err); }
      res.json(doc);
    });
  }

  getPaginate = (req , res) => {
    const page = parseInt(req.query.page) + 1;
    if(req.query.priceOnly == 'true'){
      req.query['price'] = {$gt: 1, $lt: 4999};
    }
    else{
      req.query['price'] = {$lt: 4999};
    }
    prepareFilters(req.query)
    this.model.paginate(req.query,{
      select: '-priceHistory -comments -audio',
      page: page
    }, (err, result) => {
      if (err) { return console.error(err); }
      res.json({rows: result.docs, page: result.page! - 1, totalPages: result.totalPages, totalDocs: result.totalDocs});
    })
  }

  //push comment
  addComment = (req, res) => {
    let comment = req.body;
    comment.rating = [];
    comment.created = Date.now();
    this.model.updateOne({ _id: req.params.id },  { $push: { comments: comment } },{upsert: true}, (err, doc) => {
      if (err) { return console.error(err); }
        res.send(comment);
    });
  }

  likeComment = (req, res) => {
    let userid = req.body.userid;
    let comment_id = req.params.comment;
    this.model.findOne({ _id: req.params.id }, (err, component) => {
      if (err) { return console.error(err); }
      let comment = component.comments.id(comment_id);
      let index = comment.rating.indexOf(userid);
      if(index == -1){
          comment.rating.push(userid);
      }
      else{
        comment.rating.splice(index,1);
      }
      component.save(function (err) {
        if (err) { return console.error(err); }
        res.sendStatus(200);
      });
    });
  }

  insert = (req, res) => {
    const obj = new this.model(req.body);
    obj.priceHistory = [];
    obj.save((err, item) => {
      if (err && err.code === 11000) {
        res.sendStatus(400);
      }
      if (err) {
        return console.error(err);
      }
      res.status(200).json(item);
    });
  }


  // Update by id
  update = (req, res) => {
    this.model.findOneAndUpdate({ _id: req.params.id }, req.body,(err, model) => {
      if (err) { return console.error(err); }
      res.send(model);
    });
  }

  // Delete by id
  delete = (req, res) => {
    this.model.findOneAndRemove({ _id: req.params.id }, (err) => {
      if (err) { return console.error(err); }
      res.sendStatus(200);
    });
  }
}

export default CommonCtrl;
