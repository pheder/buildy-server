import Mobo from '../models/mobo';
import CommonCtrl from './common';
import { prepareFilters } from '../utils';


export default class cc extends CommonCtrl  {
  model = Mobo;

  getAll = (req, res) => {
    this.model.find({}, 'manufacturer model price socket form chipset ram_size pcie retailers images wifi sata ram_type ram_slots audio m2' ,(err, docs) => {
      if (err) { return console.error(err); }
      res.json(docs);
    });
  }
}
