import Build from '../models/build';
import CommonCtrl from './common';


export default class cc extends CommonCtrl  {
  model = Build;


  getAll = (req, res) => {
    this.model.find({} ,(err, docs) => {
      if (err) { return console.error(err); }
      res.json(docs);
    });
  }

  insert = (req, res) => {
    const obj = new this.model(req.body);
    obj.priceHistory = [];
    obj.save((err, item) => {
      if (err && err.code === 11000) {
        res.sendStatus(400);
      }
      if (err) {
        return console.error(err);
      }
      res.status(200).json(item);
    });
  }

  // Get by id
  get = (req, res) => {
    this.model.findOne({ _id: req.params.id }, (err, obj) => {
      if (err) { return console.error(err); }
      res.json(obj);
    });
  }

  // Update by id
  update = (req, res) => {
    this.model.findOneAndUpdate({ _id: req.params.id }, req.body,(err, model) => {
      if (err) { return console.error(err); }
      res.send(model);
    });
  }

  // Delete by id
  delete = (req, res) => {
    this.model.findOneAndRemove({ _id: req.params.id }, (err) => {
      if (err) { return console.error(err); }
      res.sendStatus(200);
    });
  }

  getUserBuilds = (req, res) => {
    console.log(req.params);
    console.log("hi")
    this.model.find({ 'owner._id': req.params.userId }, (err, docs) => {
      if (err) { return console.error(err); }
      res.json(docs);
    });
  }


}
