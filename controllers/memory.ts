import Memory from '../models/memory';
import CommonCtrl from './common';
import { prepareFilters } from '../utils';


export default class cc extends CommonCtrl  {
  model = Memory;


  getAll = (req, res) => {
    this.model.find({}, 'manufacturer model price speed size type retailers images' ,(err, docs) => {
      if (err) { return console.error(err); }
      res.json(docs);
    });
  }

}
