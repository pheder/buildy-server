import * as express from 'express';

import CpuCtrl from './controllers/cpu';
import UserCtrl from './controllers/user';
import ComponentCtrl from './controllers/component';
import GpuController from './controllers/gpu';
import DriveController from './controllers/drive';
import MoboController from './controllers/mobo';
import MemoryController from './controllers/memory';
import PsuController from './controllers/psu';
import CaseController from './controllers/case';
import CpuCoolerController from './controllers/cpu-cooler'

import BuildController from './controllers/build';


export default function setRoutes(app: express.Application) {

  const router = express.Router();

  const cc = new ComponentCtrl();
  const cpuController = new CpuCtrl();
  const userCtrl = new UserCtrl();
  const gpuCtrl = new GpuController();
  const driveCtrl = new DriveController();
  const moboCtrl = new MoboController();
  const memoryCtrl = new MemoryController();
  const psuCtrl = new PsuController();
  const caseCtrl = new CaseController();
  const CpuCoolerCtrl = new CpuCoolerController();

  const buildCtrl = new BuildController();


  //Validation routes

  router.post('/components',userCtrl.validateUser);
  router.post('/cpu',userCtrl.validateUser);
  router.put('/cpu',userCtrl.validateUser);
  router.post('/gpu',userCtrl.validateUser);
  router.put('/gpu',userCtrl.validateUser);
  router.post('/drive',userCtrl.validateUser);
  router.put('/drive',userCtrl.validateUser);
  router.post('/motherboard',userCtrl.validateUser);
  router.put('/motherboard',userCtrl.validateUser);
  router.post('/memory',userCtrl.validateUser);
  router.put('/memory',userCtrl.validateUser);
  router.post('/psu',userCtrl.validateUser);
  router.put('/psu',userCtrl.validateUser);
  router.post('/case',userCtrl.validateUser);
  router.put('/case',userCtrl.validateUser);
  router.post('/cpu-cooler',userCtrl.validateUser);
  router.put('/cpu-cooler',userCtrl.validateUser);
  router.post('/build',userCtrl.validateUser);
  router.put('/build',userCtrl.validateUser);


  router.route('/components').get(cc.getAll);
  router.route('/components/count').get(cc.count);
  router.route('/components').post(cc.insert);
  router.route('/components/:id').get(cc.get);
  router.route('/components/:id').put(cc.update);
  router.route('/components/:id').delete(cc.delete);

  router.route('/components/validate').post(cc.validate);
  router.route('/components/upload').post(cc.upload);

  //CPU

  router.route('/cpu').get(cpuController.getPaginate);
  router.route('/cpu').post(cpuController.insert);
  router.route('/cpu/:id').get(cpuController.get);
  router.route('/cpu/:id').put(cpuController.update);
  //router.route('/cpu/:id').delete(cpuController.delete);
  router.route('/cpu/:id/comment').post(cpuController.addComment);
  router.route('/cpu/:id/comment/:comment').put(cpuController.likeComment);

//GPU

router.route('/gpu').get(gpuCtrl.getPaginate);
router.route('/gpu').post(gpuCtrl.insert);
router.route('/gpu/:id').get(gpuCtrl.get);
router.route('/gpu/:id').put(gpuCtrl.update);
//router.route('/gpu/:id').delete(gpuCtrl.delete);
router.route('/gpu/:id/comment').post(gpuCtrl.addComment);
router.route('/gpu/:id/comment/:comment').put(gpuCtrl.likeComment);

//DRIVE

router.route('/drive').get(driveCtrl.getPaginate);
router.route('/drive').post(driveCtrl.insert);
router.route('/drive/:id').get(driveCtrl.get);
router.route('/drive/:id').put(driveCtrl.update);
//router.route('/drive/:id').delete(driveCtrl.delete);
router.route('/drive/:id/comment').post(driveCtrl.addComment);
router.route('/drive/:id/comment/:comment').put(driveCtrl.likeComment);

//MOTHERBOARD

router.route('/motherboard').get(moboCtrl.getPaginate);
router.route('/motherboard').post(moboCtrl.insert);
router.route('/motherboard/:id').get(moboCtrl.get);
router.route('/motherboard/:id').put(moboCtrl.update);
//router.route('/motherboard/:id').delete(moboCtrl.delete);
router.route('/motherboard/:id/comment').post(moboCtrl.addComment);
router.route('/motherboard/:id/comment/:comment').put(moboCtrl.likeComment);

//MEMORY

router.route('/memory').get(memoryCtrl.getPaginate);
router.route('/memory').post(memoryCtrl.insert);
router.route('/memory/:id').get(memoryCtrl.get);
router.route('/memory/:id').put(memoryCtrl.update);
//router.route('/memory/:id').delete(memoryCtrl.delete);
router.route('/memory/:id/comment').post(memoryCtrl.addComment);
router.route('/memory/:id/comment/:comment').put(memoryCtrl.likeComment);

// POWER SUPPLY
router.route('/psu').get(psuCtrl.getPaginate);
router.route('/psu').post(psuCtrl.insert);
router.route('/psu/:id').get(psuCtrl.get);
router.route('/psu/:id').put(psuCtrl.update);
//router.route('/psu/:id').delete(psuCtrl.delete);
router.route('/psu/:id/comment').post(psuCtrl.addComment);
router.route('/psu/:id/comment/:comment').put(psuCtrl.likeComment);

// CASE
router.route('/case').get(caseCtrl.getPaginate);
router.route('/case').post(caseCtrl.insert);
router.route('/case/:id').get(caseCtrl.get);
router.route('/case/:id').put(caseCtrl.update);
//router.route('/case/:id').delete(caseCtrl.delete);
router.route('/case/:id/comment').post(caseCtrl.addComment);
router.route('/case/:id/comment/:comment').put(caseCtrl.likeComment);

// CPU COOLER
router.route('/cpu-cooler').get(CpuCoolerCtrl.getPaginate);
router.route('/cpu-cooler').post(CpuCoolerCtrl.insert);
router.route('/cpu-cooler/:id').get(CpuCoolerCtrl.get);
router.route('/cpu-cooler/:id').put(CpuCoolerCtrl.update);
//router.route('/cpu-cooler/:id').delete(CpuCoolerCtrl.delete);
router.route('/cpu/:id/comment').post(cpuController.addComment);
router.route('/cpu/:id/comment/:comment').put(cpuController.likeComment);

// Builds
router.route('/build').get(buildCtrl.getPaginate);
router.route('/build').post(buildCtrl.insert);
router.route('/build/:id').get(buildCtrl.get);
router.route('/build/:id').put(buildCtrl.update);
router.route('/build/user/:userId').get(buildCtrl.getUserBuilds);
//router.route('/build/:id').delete(buildCtrl.delete);

// Users
router.route('/login').post(userCtrl.login);
//router.route('/users').get(userCtrl.getAll); i dont want this in prod
router.route('/users/count').get(userCtrl.count);
router.route('/user').post(userCtrl.insert);
router.route('/userinfo').get(userCtrl.getUserInfo);
router.route('/user/:id').get(userCtrl.get);
router.route('/user/:id/build').get(userCtrl.getBuild);
router.route('/user/:id/build').post(userCtrl.updateBuild);
router.route('/user/:id/build/:type/:componentId').delete(userCtrl.deleteBuildComponent);
router.route('/user/:id/buildName').post(userCtrl.updateBuildName);
router.route('/user/:id/build/finish').post(userCtrl.finishBuild);
router.route('/user/:id').put(userCtrl.update);
router.route('/user/:id').delete(userCtrl.delete);


// Apply the routes to our application with the prefix /api
app.use('/api', router);

}
