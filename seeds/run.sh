mongoimport --db app --collection cpus --file cpus.json  --jsonArray
mongoimport --db app --collection cpu-coolers --file cooler.json  --jsonArray
mongoimport --db app --collection cases --file cases.json  --jsonArray
mongoimport --db app --collection drives --file drives.json  --jsonArray
mongoimport --db app --collection gpus --file gpus.json  --jsonArray
mongoimport --db app --collection memories --file memory.json  --jsonArray
mongoimport --db app --collection psus --file psu.json  --jsonArray
mongoimport --db app --collection motherboards --file mobos.json  --jsonArray