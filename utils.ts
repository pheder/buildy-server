import * as bcrypt from 'bcryptjs';


// Method to validate login password
export const comparePassword = (inputPassword, userPassword, callback) => {
  bcrypt.compare(inputPassword, userPassword, function (err, isMatch) {
    if (err) {
      return callback(err);
    }
    callback(null, isMatch);
  });
}

// Method to parse the filters to query the database
export function prepareFilters(query: any) {
  delete query.page;
  delete query.priceOnly;
  Object.entries(query).forEach(element => {
    if (typeof element[1] === 'string') {
      query[element[0]] = new RegExp(element[1], 'i')
    }
  })
}

// Proxy list for scrapping
export const proxies = [
  'http://54.37.84.141:3128'
];

// Valid component list
export const componentList = [
  'cpu',
  'gpu',
  'cooler',
  'motherboard',
  'case',
  'drive',
  'psu',
  'memory'
]