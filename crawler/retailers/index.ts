import Crawler = require('crawler');
import * as cheerio from 'cheerio';
import * as fs from 'fs';
import {
    proxies
} from '../../utils'
import {
    matchComponent
} from '../filters';

let total = 0;

// Crawler for Pc Componentes retailer
export const pccomponentesCrawler = (components: any) => new Crawler({
    rateLimit: 1000, 
    retries: 10, // Number of retries
    maxConnections: 1, // Max concurrent connections
    headers: {
        'host': 'www.pccomponentes.com',
        'Accept': '*/*',
        'User-Agent': 'Mozilla/5.0 (compatible; Rigor/1.0.0; http://rigor.com)'
    },
    callback: function (error, res, done) {
        if (error) {
            console.log(error);
        } else {
            var $ = cheerio.load(res.body);
            var selector = $('article.tarjeta-articulo');
            var items = selector.length;
            console.log(`NUM ITEMS PCCOMPONENTES: ${items}`);
            selector.each((index, ele) => {
                let element = $(ele).find('a.enlace-disimulado');
                let name = element.attr('data-name');
                let url = res.request.uri.protocol + "//" + res.request.uri.host + "" + element.attr('href');
                let price = parseFloat(element.attr('data-price'));
                let stock = parseInt(element.attr('data-stock-web')) < 4;
                let found = components.find((comp) => {
                    const retailer = comp.retailers.find(r => r.name === 'pccomponentes');
                    if (retailer) {
                        return retailer.url === url;
                    } else {
                        return matchComponent(comp, name);
                    }
                });
                if (found) {
                    console.log(`COMPONENT MATCH : ${found.model} = ${name}`);
                    found.updated = true;
                    const index = found.retailers.findIndex(r => r.name === 'pccomponentes');
                    if (index === -1) {
                        found.retailers.push({
                            name: 'pccomponentes',
                            price: price,
                            stock: stock,
                            url: url
                        });
                    } else {
                        found.retailers[index].price = price;
                    }

                    /*if(found.images.length === 0){
                        let imgPath = 'https:' + $(ele).find('img').attr('src').replace('w-220-220','w-530-530');
                        let extension = imgPath.split('.').pop();
                        let filename =  found.model + '.' + extension;
                        found.images = [filename];
                        var proxy = proxies[Math.floor(Math.random() * proxies.length)];
                        imgCrawler.queue({uri: imgPath, filename:filename, proxy: proxy});
                        let imgPath = 'https:' + $(this).find('img').attr('src');
                        let extension = imgPath.split('.').pop();
                        let filename =  found.model + '.' + extension;
                        found.images = [filename];
                    }*/

                } else {
                    console.log(`COMPONENT NOT MATCH : ${name}`);
                }

            });


            if (items === 24) {
                console.log(`LOAD PAGE`);
                total += 24;
                var pageNumber = parseInt(res.request.uri.query.split("=")[1]) + 1;
                var target = res.request.uri.href.replace(/page=[0-9]+/, 'page=' + pageNumber);
                console.log(target);
                var proxy = proxies[Math.floor(Math.random() * proxies.length)];
                pccomponentesCrawler(components).queue({
                    uri: target,
                    proxy: proxy
                });

            } else {
                total += items;
            }

            done();
        }
    }
});

// Crawler for Xtremmedia retailer
export const xmmediaCrawler = (components: any) => new Crawler({
    rateLimit: 500,
    retries: 10,
    maxConnections: 1,
    headers: {
        'Content-Type': 'multipart/form-data',
        'User-Agent': 'Mozilla/5.0 (compatible; Rigor/1.0.0; http://rigor.com)',
    },
    callback: function (error, res, done) {
        if (error) {
            console.log(error);
        } else {
            var parsed = JSON.parse(res.body);
            var $ = cheerio.load(parsed.content);
            var selector = $('.article-list');
            var items = selector.length;
            console.log(`NUM ITEMS XTREMMEDIA: ${items}`);

            selector.each((index, ele) =>  {
                let link = $(ele).find('.article-list-titulo > a');
                let link_url = link.attr('href');
                let name = link.text();
                let url = res.request.uri.protocol + "//" + res.request.uri.host + "" + link_url.substring(1, link_url.length);
                let price : number | null = parseFloat($(ele).find('.article-list-pvp').text().replace(',', '.'));
                const stock = $(ele).find('.article-list-sin_stock').length === 0;
                if (isNaN(price)) {
                    price = null;
                }
                let found = components.find((comp) => {
                    const retailer = comp.retailers.find(r => r.name === 'xtremmedia');
                    if (retailer) {
                        return retailer.url === url;
                    } else {
                        return matchComponent(comp, name);
                    }
                });
                if (found) {
                    console.log(`COMPONENT MATCH : ${found.model} = ${name}`);
                    found.updated = true;
                    const index = found.retailers.findIndex(r => r.name === 'xtremmedia');
                    if (index === -1) {
                        found.retailers.push({
                            name: 'xtremmedia',
                            price: price,
                            stock: stock,
                            url: url
                        });
                    } else {
                        found.retailers[index].price = price;
                    }
                } else {
                    console.log(`COMPONENT NOT MATCH : ${name}`);
                }

            });

            done();

        }
    }
});

// Crawler for Coolmod retailer
export const cmCrawler = (components: any) => new Crawler({
    rateLimit: 500,
    retries: 10,
    maxConnections: 1,
    headers: {
        'Host': 'www.coolmod.com',
        'X-Requested-With': 'XMLHttpRequest',
        'Origin': 'https://www.coolmod.com'
    },
    callback: function (error, res, done) {
        if (error) {
            console.log(error);
        } else {
            var $ = cheerio.load(res.body);
            var selector = $('.product.item-product');
            var items = selector.length;
            console.log(`NUM ITEMS COOLMOD: ${items}`);
            selector.each( (index, ele) => {
                let link = $(ele).find('.product-name > a');
                let link_url = link.attr('href');
                let name = link.attr('title');
                let url = res.request.uri.protocol + "//" + res.request.uri.host + "" + link_url;
                let price = parseFloat($(ele).find('.mod-product-price').text().replace('.', '').replace(',', '.'));
                console.log(`COOLMOD PRICE: ${price}`);
                let stock = $(ele).find('.product-availability.cat-product-availability').text().indexOf('Sin') === -1;
                let found = components.find((comp) => {
                    const retailer = comp.retailers.find(r => r.name === 'coolmod');
                    if (retailer) {
                        return retailer.url === url;
                    } else {
                        return matchComponent(comp, name);
                    }
                });
                if (found) {
                    console.log(`COMPONENT MATCH : ${found.model} = ${name}`);
                    found.updated = true;
                    const index = found.retailers.findIndex(r => r.name === 'coolmod');
                    if (index === -1) {
                        found.retailers.push({
                            name: 'coolmod',
                            price: price,
                            stock: stock,
                            url: url
                        });
                    } else {
                        found.retailers[index].price = price;
                    }
                } else {
                    console.log(`COMPONENT NOT MATCH : ${name}`);
                }
            });
            done();
        }
    }
});

// Crawler for image downloading
const imgCrawler = new Crawler({
    encoding: null,
    jQuery: false,
    callback: function (err, res, done) {
        if (err) {
            console.error(err.stack);
        } else {
            fs.createWriteStream('images/' + res.options.filename).write(res.body);
        }

        done();
    }
});
