import Cpu from "../../models/cpu";
import Gpu from "../../models/gpu";
import CpuCooler from "../../models/cpu-cooler";
import Mobo from "../../models/mobo";
import Case from "../../models/case";
import Drive from "../../models/drive";
import Psu from "../../models/psu";
import Memory from "../../models/memory";

let regex;
let color;

// Try to match components by name
export const matchComponent = (comp, target ): boolean => {
    switch(comp.constructor.modelName){
        case 'Cpu':
            regex = new RegExp(comp.model + '($|\\s)','gi');
            return regex.test(target);
        break;
        case 'Gpu':
            const chipset = new RegExp(/(\d{2,5}|titan)(\s\w+)?/gi).exec(comp.chipset)![0];
            return (new RegExp(comp.model + '($|\\s)','gi').test(target) && 
            new RegExp(comp.manufacturer + '($|\\s)','gi').test(target) && 
            new RegExp(chipset + '($|\\s)','gi').test(target));            
        break;
        case 'Case':
            color = hasColor(target)
            return  (new RegExp(comp.model + '($|\\s)','gi').test(target) && 
            new RegExp(comp.manufacturer + '($|\\s)','gi').test(target) && 
            (color ? new RegExp(getColor(comp.color) + '($|\\s)','gi').test(target) : true))
        break;
        case 'Cpu-cooler':
            return  (new RegExp(comp.model + '($|\\s)','gi').test(target) && 
            new RegExp(comp.manufacturer + '($|\\s)','gi').test(target))
        break;
        case 'Drive':
            const capacity = comp.capacity > 1000 ? (comp.capacity / 1024) + 'TB' : comp.capacity + 'GB';
            if(new RegExp('ssd($|\\s)','gi').test(target)){
                // SSD DISK
                return  (new RegExp(comp.model + '($|\\s)','gi').test(target) && 
                new RegExp(comp.manufacturer + '($|\\s)','gi').test(target) && 
                new RegExp(capacity + '($|\\s)','gi').test(target) )
            }
            else{
               // HDD DISK
               return  (new RegExp(comp.model + '($|\\s)','gi').test(target) && 
               new RegExp(comp.manufacturer + '($|\\s)','gi').test(target) && 
               new RegExp(capacity + '($|\\s)','gi').test(target)  && 
               new RegExp(comp.size.slice(0, comp.size.length-1) + '($|\\s)','gi').test(target) )
            }
        break;
        case 'Memory':
           const name = comp.model.split(/\s(?=\d{1,2}\s?GB)/gi)[0]
           const size = comp.model.split(/\s(?=\d{1,2}\s?GB)/gi)[1].replace(" ", "");
           const speed = comp.speed.split("-")[1];
           color = hasColor(target)

           return  (new RegExp(name + '($|\\s)','gi').test(target) && 
           new RegExp(comp.manufacturer + '($|\\s)','gi').test(target) && 
           new RegExp(size + '($|\\s)','gi').test(target)  && 
           new RegExp(speed + '($|\\s)','gi').test(target) &&
           (color ? new RegExp(getColor(comp.color) + '($|\\s)','gi').test(target) : true));
        break;
        case 'Motherboard':
            return  (new RegExp(comp.model + '($|\\s)','gi').test(target) && 
            new RegExp(comp.manufacturer + '($|\\s)','gi').test(target))
        break;
        case 'Psu':
            return  (new RegExp(comp.model + '($|\\s)','gi').test(target) && 
            new RegExp(comp.manufacturer + '($|\\s)','gi').test(target) && 
            new RegExp(comp.power + '($|\\s)','gi').test(target))
        break;
        default: 
        return false;        
    }
}

// Try to match component color by color name
const getColor = (color: string) => {
    const primary = color.split("/")[0].trim().toLowerCase();
    switch(primary){
        case 'beige':
            return 'beige'
        break;
        case 'black':
            return 'negr'
        break;
        case 'blue':
            return 'azul'
        break;
        case 'camo':
            return 'camo'
        break;
        case 'gold':
            return 'dorad'
        break;
        case 'gray':
            return 'gris'
        break;
        case 'green':
            return 'verde'
        break;
        case 'multicolor':
            return 'multi'
        break;
        case 'orange':
         return 'naranja'
        break;
        case 'pink':
            return 'rosa'
        break;
        case 'red':
            return 'roj'
        break;
        case 'silver':
            return 'plat'
        break;
        case 'white':
            return 'blanc'
        break;
        case 'yellow':
            return 'amarill'
        break;
    }
}

// Return a component list by type
export const getComponentList = (type: string) => {
    switch(type){
        case 'cpu':
             return Cpu.find({}).exec();
        break;
        case 'gpu':
             return Gpu.find({}).exec();
        break;
        case 'cooler':
             return CpuCooler.find({}).exec();
        break;
        case 'motherboard':
             return Mobo.find({}).exec();
        break;
        case 'case':
             return Case.find({}).exec();
        break;
        case 'drive':
             return Drive.find({}).exec();
        break;
        case 'psu':
             return Psu.find({}).exec();
        break;
        case 'memory':
             return Memory.find({}).exec();
        break;
    }
}

// Check if a product has a color in the model
const hasColor = (name) => {
    return validColors.some( c => c === name.toLowerCase());
}

// Valid colors
const validColors = ['negr', 'azul', 'verd', 'amarill', 'blanc', 'plat', 'rosa', 'gris', 'dorad']