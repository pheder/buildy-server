
import {
    proxies
} from '../utils'
import {
    pccomponentesCrawler,
    cmCrawler,
    xmmediaCrawler
} from './retailers';
import { getComponentList } from './filters';


// Crawl all components from a type
export const crawlComponent = (type): Promise < any > => {
    return new Promise(async (resolve, reject) => {

        // Load component list from the database
        const components = await getComponentList(type);

        // Set Retailer crawlers
        const pcCrawler = pccomponentesCrawler(components);
        const coolmodCrawler = cmCrawler(components);
        const xmCrawler = xmmediaCrawler(components);
      
        // Set crawler parameters according to the component type
        setPcQueue(pcCrawler, type);
        setXtremmediaQueue(xmCrawler, type);
        setCoolmodQueue(coolmodCrawler, type);

        // Set promises that listen until crawling ends
        const pccPromise = new Promise(resolve => {
            pcCrawler.on('drain', function () {
                resolve('DONE PC COMPONENTES')
            });
        });
        const cmPromise = new Promise(resolve => {
            coolmodCrawler.on('drain', function () {
                resolve('DONE COOLMOD')
            });
        });
        const xmPromise = new Promise(resolve => {
            xmCrawler.on('drain', function () {
                resolve('DONE xtremmedia')
            });
        });

        // Update all components that were modified
        return Promise.all([pccPromise, cmPromise, xmPromise ]).then(res => {
            let d = new Date();
            d.setHours(0, 0, 0, 0)
            let indexHistory, minPrice;
            components!.forEach(async comp => {
                if (comp.updated) {
                    indexHistory = comp.priceHistory.findIndex(h => h.day.getTime() === d.getTime());
                    if (indexHistory === -1) comp.priceHistory.push({
                        day: d,
                        retailers: comp.retailers
                    });
                    else{
                        comp.priceHistory[comp.priceHistory.length - 1].retailers = comp.retailers;
                    }
                    minPrice = 50000; 
                    comp.retailers.forEach(r => {
                        if (r.price != null && r.price < minPrice) {
                            minPrice = r.price;
                        }
                    });
                    comp.price = minPrice;
                    try{
                        await comp.save()
                    }
                    catch(err){
                        console.log(err);
                    }
                }
            });
            resolve('DONE ' + type )
        })

    })
}

const setPcQueue = (crawler, type) =>{
    const proxy = proxies[Math.floor(Math.random() * proxies.length)]
    switch(type){
        case 'cpu':
            crawler.queue({
                uri: 'https://www.pccomponentes.com/listado/ajax?page=0&order=relevance&idFamilies[]=4',
                proxy: proxy
            });
        break;
        case 'gpu':
            crawler.queue({
                uri: 'https://www.pccomponentes.com/listado/ajax?page=0&idFilters[]=5181&idFilters[]=5058&idFilters[]=4960&idFilters[]=4864&idFilters[]=2840&idFilters[]=693&idFamilies[]=6',
                proxy: proxy
            });
        break;
        case 'cooler':
        crawler.queue({
            uri: 'https://www.pccomponentes.com/listado/ajax?page=0&idFilters[]=3428&idFilters[]=2306&idFamilies[]=154',
            proxy: proxy
        });
        break;
        case 'motherboard':
        crawler.queue({
            uri: 'https://www.pccomponentes.com/listado/ajax?page=0&idFilters[]=2616&idFilters[]=4641&idFilters[]=5030&idFilters[]=3429&idFilters[]=4022&idFamilies[]=3',
            proxy: proxy
        });
        break;
        case 'case':
        crawler.queue({
            uri: 'https://www.pccomponentes.com/listado/ajax?page=0&idTrademarks[]=35&idTrademarks[]=569&idTrademarks[]=75&idTrademarks[]=231&idTrademarks[]=154&idTrademarks[]=474&idTrademarks[]=599&idTrademarks[]=425&idTrademarks[]=267&idTrademarks[]=612&idTrademarks[]=419&idTrademarks[]=113&idFilters[]=533&idFamilies[]=191',
            proxy: proxy
        });
        break;
        case 'drive':
            crawler.queue({
                uri: 'https://www.pccomponentes.com/listado/ajax?page=0&idTrademarks[]=486&idTrademarks[]=92&idTrademarks[]=226&idTrademarks[]=27&idTrademarks[]=56&idTrademarks[]=51&idTrademarks[]=195&idFilters[]=2742&idFamilies[]=5',
                proxy: proxy
            });
        break;
        case 'psu':
        crawler.queue({
            uri: 'https://www.pccomponentes.com/listado/ajax?page=0&idFilters[]=2678&idFilters[]=2718&idFamilies[]=144',
            proxy: proxy
        });            
        break;
        case 'memory':
        crawler.queue({
            uri: 'https://www.pccomponentes.com/listado/ajax?page=0&idTrademarks[]=231&idTrademarks[]=486&idTrademarks[]=364&idTrademarks[]=92&idFilters[]=4&idFilters[]=5&idFilters[]=6&idFilters[]=1143&idFilters[]=1155&idFilters[]=1156&idFilters[]=1157&idFilters[]=1324&idFilters[]=1158&idFilters[]=1278&idFilters[]=1409&idFilters[]=2292&idFilters[]=2225&idFilters[]=2299&idFilters[]=2300&idFilters[]=2385&idFilters[]=2386&idFilters[]=2464&idFilters[]=4137&idFilters[]=1262&idFamilies[]=7',
            proxy: proxy
        });
        break;
    }
}

const setXtremmediaQueue = (crawler, type) =>{
    const xmediaForm = {
        _referer: 'product/view/familia/64',
        mod: 'product',
        _validation_list: 'Ss5D9axWPe/X0GZO39pcdJIZ/ii4fbaZkjNMXMdyWgDPtR8=',
        _validation_seal: 'a4d720ff47914b1112af04b438b24341935959fa23c392ed28cedf212a1e7df7',
        req: 'search_post',
        par: '?q=product/search/familia/64/sordenacion/tven/feature/1/nview/5000',
        smartsile_seal: '75e9ea5a1538f4130158da96312be90675d2c6056500e22b2639593a80bba664',
        nview: 5000,
    };
    
    const proxy = proxies[Math.floor(Math.random() * proxies.length)]
    switch(type){
        case 'cpu':
            xmediaForm._referer = 'product/view/familia/31';
            xmediaForm.par = '?q=product/search/familia/31/sordenacion/tven/feature/1/nview/5000';
        break;
        case 'gpu':
            xmediaForm._referer = 'product/view/familia/64';
            xmediaForm.par = '?q=product/search/familia/64/sordenacion/tven/feature/1/nview/5000';
        break;
        case 'cooler':
            xmediaForm._referer = 'product/view/familia/54';
            xmediaForm.par = '?q=product/search/familia/54';
        break;
        case 'motherboard':
            xmediaForm._referer = 'product/view/familia/30';
            xmediaForm.par = '?q=product/search/familia/30';
        break;
        case 'case':
            xmediaForm._referer = 'product/view/familia/28';
            xmediaForm.par = '?q=product/search/familia/28';
        break;
        case 'drive':
            xmediaForm._referer = 'product/view/familia/32';
            xmediaForm.par = '?q=product/search/familia/32';
        break;
        case 'psu':
            xmediaForm._referer = 'product/view/familia/29';
            xmediaForm.par = '?q=product/search/familia/29';
        break;
        case 'memory':
            xmediaForm._referer = 'product/view/familia/62';
            xmediaForm.par = '?q=product/search/familia/62';
        break;
    }

    crawler.queue({
        uri: 'https://xtremmedia.com/?s=product/view',
        method: 'POST',
        formData: xmediaForm,
        followAllRedirects: true,
        proxy: proxy,
    });
}


const setCoolmodQueue = (crawler, type) =>{
   
    const cmForm = {
        'limits[0]': '0',
        'limits[1]': '5000',
        'id': '1773',
    };

    const proxy = proxies[Math.floor(Math.random() * proxies.length)]
    switch(type){
        case 'cpu':
            cmForm.id = '1769';
        break;
        case 'gpu':
            cmForm.id = '1773';
        break;
        case 'cooler':
            cmForm.id = '1491';
        break;
        case 'motherboard':
            cmForm.id = '1767';
        break;
        case 'case':
            cmForm.id = '1509';
        break;
        case 'drive':
             cmForm.id = '1902'; // ssd 2127
        break;
        case 'psu':
            cmForm.id = '1495';
        break;
        case 'memory':
            cmForm.id = '1759';
        break;
    }

    crawler.queue({
        uri: 'https://www.coolmod.com/web/vistaCategorias/load_ajax_products',
        method: 'POST',
        formData: cmForm,
        proxy: proxy,
    });
}