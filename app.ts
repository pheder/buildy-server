// Server main libraries

import * as bodyParser from 'body-parser';
import express from 'express';
import * as mongoose from 'mongoose';
import * as path from 'path';
import * as dotenv from 'dotenv';
import * as https from 'https';
import * as fs from 'fs';

// Config env variables
dotenv.config();

import setRoutes from './routes';
import {crawlComponent} from './crawler';
import { componentList } from './utils';

// Run express server

const app: express.Application = express()

// Certificate
const privateKey = fs.readFileSync('/etc/letsencrypt/live/buildy.tk/privkey.pem', 'utf8');
const certificate = fs.readFileSync('/etc/letsencrypt/live/buildy.tk/cert.pem', 'utf8');
const ca = fs.readFileSync('/etc/letsencrypt/live/buildy.tk/chain.pem', 'utf8');

const credentials = {
	key: privateKey,
	cert: certificate,
	ca: ca
};

const httpsServer = https.createServer(credentials, app);

httpsServer.listen(443, () => {
	console.log('HTTPS Server running on port 443');
});

app.set('port', (process.env.PORT || 3000));

app.use(express.static(__dirname, { dotfiles: 'allow' } ));
// app.use('/', express.static(path.join(__dirname, '../public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*'); // * => allow all origins
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, X-Auth-Token, Accept'); // add remove headers according to your needs
  next()
})

// Connect to mongo database
mongoose.connect(process.env.MONGODB_URI as string, {useNewUrlParser: true, useCreateIndex: true});
const db = mongoose.connection;
(<any>mongoose).Promise = global.Promise;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => {
  console.log('Connected to MongoDB');
  setRoutes(app);

  app.listen(app.get('port'), () => {
    console.log('Server UP on port ' + app.get('port'));
  });


//  scrapeComponents();

});


// Handle unexpected errors
process.on('uncaughtException', (err) => {
  console.log(err.name);
  console.log(err.message);
  console.log(err.stack);
});

process.on('unhandledRejection', (reason, p) => {
  console.log('Unhandled Rejection at:', p, 'reason:', reason);
});


// Main function to scrape components
async function scrapeComponents(){
  for(const type of componentList){
    await crawlComponent(type).then(res => {
      console.log(res)
    }, err =>{
      console.log(err);
    })
  }
}

export { app };
