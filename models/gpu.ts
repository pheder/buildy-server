import * as mongoose from 'mongoose';

import baseComponentSchema from './component';
import mongoosePaginate = require('mongoose-paginate-v2');

const gpuSchema = new mongoose.Schema( Object.assign({}, baseComponentSchema.obj,{
  chipset: String,
  memory_size: Number,
  memory_type: String,
  ports: String,
  clock: Number,
  boost_clock: Number,
  tdp: Number,
  sync: String,
})).plugin(mongoosePaginate);

const Gpu = mongoose.model('Gpu', gpuSchema);

export default Gpu;
