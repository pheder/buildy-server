import * as mongoose from 'mongoose';

import baseComponentSchema from './component';
import mongoosePaginate = require('mongoose-paginate-v2');

const memorySchema = new mongoose.Schema( Object.assign({}, baseComponentSchema.obj,{
  speed: String,
  size: String,
  type: String,
  color: String,
})).plugin(mongoosePaginate);

const Memory = mongoose.model('Memory', memorySchema);

export default Memory;
