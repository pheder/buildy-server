import * as mongoose from 'mongoose';

import baseComponentSchema from './component';
import mongoosePaginate = require('mongoose-paginate-v2');

const caseSchema = new mongoose.Schema(
  Object.assign({}, baseComponentSchema.obj, {
    type: String,
    exBig: Number,
    inBig: Number,
    inSmall: Number,
    cardSize: Number,
    color: String,
  })
).plugin(mongoosePaginate);

const Case = mongoose.model('Case', caseSchema);

export default Case;
