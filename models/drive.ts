import * as mongoose from 'mongoose';

import baseComponentSchema from './component';
import mongoosePaginate = require('mongoose-paginate-v2');

const driveSchema = new mongoose.Schema( Object.assign({}, baseComponentSchema.obj,{
  size: String,
  type: String,
  capacity: Number,
  interface: String,
  })
).plugin(mongoosePaginate);

const Drive = mongoose.model('Drive', driveSchema);

export default Drive;
