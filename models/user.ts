import * as bcrypt from 'bcryptjs';
import {Schema, Document, Model, model} from 'mongoose';


export interface IUser extends Document {
  email: string;
  username: string;
  password: string;
  build: any;
}

const userSchema = new Schema({
  username: String,
  email: { type: String, unique: true, lowercase: true, trim: true },
  password: String,
  role: String,
  build: {
    name: String,
    description: String,
    components: {
      cpu: {_id: String, manufacturer: String, model: String, price: Number},
      cpu_cooler: {_id: String, manufacturer: String, model: String, price: Number},
      gpu: {_id: String, manufacturer: String, model: String, price: Number},
      case: {_id: String, manufacturer: String, model: String, price: Number},
      drive: [{_id: String, manufacturer: String, model: String, price: Number}],
      motherboard: {_id: String, manufacturer: String, model: String, price: Number},
      psu: {_id: String, manufacturer: String, model: String, price: Number},
      memory: {_id: String, manufacturer: String, model: String, price: Number},
    },
    price: Number,
    _id: false
  }
});

// Before saving the user, hash the password
userSchema.pre('save', function(next) {
  const user: any = this;
  user.role = "user";
  if (!user.isModified('password')) { return next(); }
  bcrypt.genSalt(10, function(err, salt) {
    if (err) { return next(err); }
    bcrypt.hash(user.password, salt, function(error, hash) {
      if (error) { return next(error); }
      user.password = hash;
      next();
    });
  });
});

userSchema.set('toJSON', {
  transform: function(doc, ret, options) {
    delete ret.password;
    return ret;
  }
});

const User = model<IUser>('User', userSchema);

export default User;
