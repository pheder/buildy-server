import * as mongoose from 'mongoose';
import mongoosePaginate = require('mongoose-paginate-v2');

const buildSchema = new mongoose.Schema({
  owner: {_id: String, name: String},
  name: String,
  description: String,
  components: {
    cpu: {_id: String, manufacturer: String, model: String, price: Number},
    cpu_cooler: {_id: String, manufacturer: String, model: String, price: Number},
    gpu: {_id: String, manufacturer: String, model: String, price: Number},
    case: {_id: String, manufacturer: String, model: String, price: Number},
    drive: [{_id: String, manufacturer: String, model: String, price: Number}],
    memory: {_id: String, manufacturer: String, model: String, price: Number},
    psu: {_id: String, manufacturer: String, model: String, price: Number},
    motherboard: {_id: String, manufacturer: String, model: String, price: Number},
  },
  price: Number,
  images: [String]
}).plugin(mongoosePaginate);

const Build = mongoose.model('Build', buildSchema);

export default Build;
