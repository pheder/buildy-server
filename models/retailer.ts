import * as mongoose from 'mongoose';

const retailerSchema = new mongoose.Schema({
  name: String,
  url: String,
  price: Number,
  stock: Boolean,
}, {_id: false});

const Retailer = mongoose.model('Retailer', retailerSchema);

export default retailerSchema;
