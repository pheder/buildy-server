import * as mongoose from 'mongoose';
import mongoosePaginate = require('mongoose-paginate-v2');

import baseComponentSchema from './component';


const cpuSchema =  new mongoose.Schema(
  Object.assign({}, baseComponentSchema.obj, {
    socket: String,
    clock: Number,
    cores: Number,
    threads: Number,
    tdp: Number,
    igpu: String,
    multithreading: Boolean
  })
).plugin(mongoosePaginate);

const Cpu = mongoose.model('Cpu', cpuSchema);

export default Cpu;
