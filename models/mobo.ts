import * as mongoose from 'mongoose';

import baseComponentSchema from './component';
import mongoosePaginate = require('mongoose-paginate-v2');

const moboSchema = new mongoose.Schema( Object.assign({}, baseComponentSchema.obj,{
  socket: String,
  form: String,
  chipset: String,
  ram_slots: Number,
  ram_size: Number,
  ram_type: String,
  sata: Number,
  wifi: Boolean,
  m2: Number,
  pcie: Number,
  audio: String,
})).plugin(mongoosePaginate);

const Mobo = mongoose.model('Motherboard', moboSchema);

export default Mobo;
