import * as mongoose from 'mongoose';

import baseComponentSchema from './component';
import mongoosePaginate = require('mongoose-paginate-v2');

const cpuCoolerSchema = new mongoose.Schema(
  Object.assign({}, baseComponentSchema.obj, {
    liquid: Boolean,
    noise: String,
    speed: String,
    sockets: [String],
  })
).plugin(mongoosePaginate);

const CpuCooler = mongoose.model('Cpu-cooler', cpuCoolerSchema);

export default CpuCooler;
