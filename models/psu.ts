import * as mongoose from 'mongoose';

import baseComponentSchema from './component';
import mongoosePaginate = require('mongoose-paginate-v2');

const PSUSchema = new mongoose.Schema( Object.assign({}, baseComponentSchema.obj,{
  efficiency: String,
  modular: String,
  power: Number,
  type: String,
  color: String,
})).plugin(mongoosePaginate);

const Psu = mongoose.model('Psu', PSUSchema);

export default Psu;
