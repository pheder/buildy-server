import * as mongoose from 'mongoose';

var Schema = mongoose.Schema;

const commentSchema = new mongoose.Schema({
  description: String,
  author: { type: Schema.Types.ObjectId, ref: 'User'},
  rating: [String],
  created: Date
});

const Comment = mongoose.model('Comment', commentSchema);

export default commentSchema;
