import * as mongoose from 'mongoose';

import retailerSchema from './retailer';
import commentSchema from './comment';

const retailerHistorySchema = new mongoose.Schema({name: String, price: Number},{_id: false});

const priceHistorySchema = new mongoose.Schema({
  day: Date , 
  retailers: [retailerHistorySchema]
}, {_id: false});

const baseComponentSchema = new mongoose.Schema({
  manufacturer: String,
  model: String,
  price: Number,
  retailers: [retailerSchema],
  images: [String],
  priceHistory: [priceHistorySchema],
  comments: [commentSchema]
});

export const BaseComponent = mongoose.model('Component', baseComponentSchema);


export default baseComponentSchema;
